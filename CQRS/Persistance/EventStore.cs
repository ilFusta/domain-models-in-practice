﻿using CQRS.Domain.Core;
using System.Collections.Generic;
using System.Linq;

namespace CQRS.Persistance {

	class EventStore : IEventStore {

		public readonly List<IDomainEvent> Events;

		public EventStore() {
			Events = new List<IDomainEvent>();
		}

		public EventStore(IEnumerable<IDomainEvent> events) {
			Events = events.ToList();
		}

		public List<IDomainEvent> GetAggregateEvents(IUniqueID aggregateID) {
			return Events.Where(x => x.AggregateID.Equals(aggregateID)).ToList();
		}

		public void AddEvent(IDomainEvent e) {
			Events.Add(e);
		}


	}
}
