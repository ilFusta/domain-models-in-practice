﻿using CQRS.Domain.Core;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Reservations {
	public class Reservation {
        private readonly ReservationState _reservation;
        private readonly Action<IDomainEvent> _publish;

        public Reservation(ReservationState state, Action<IDomainEvent> publish) {
            _reservation = state;
            _publish = publish;
        }

       public void ReserveSeats(CustomerID customerID, ScreeningID screeningID, IEnumerable<SeatID> requestedSeats) {
            //Check screening existance? or do it in command handler?
           //Check requested seats availablility
           
	   }
    }
}
