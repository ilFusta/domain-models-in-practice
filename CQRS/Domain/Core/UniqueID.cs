﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Core {
	public class UniqueID: IUniqueID {
		private Guid ID { get; set; }
		
        public UniqueID() {
			ID = Guid.NewGuid();
		}
        public UniqueID(string id) {
            
            if (Guid.TryParse(id, out Guid g)) {
                ID = g;
			} else {
                throw new Exception("Wrong id format");
			}

        }

		public override string ToString() {
			return ID.ToString();
		}

		public override bool Equals(object ob) {
            if (ob is UniqueID) {
                UniqueID c = (UniqueID)ob;
                return c.ID.ToString() == ID.ToString();
            } else {
                return false;
            }
        }

        public override int GetHashCode() {
            return ID.GetHashCode();
        }

    }
}
