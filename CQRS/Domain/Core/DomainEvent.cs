﻿using CQRS.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Core {
	public class DomainEvent : IDomainEvent {
		public IUniqueID AggregateID { get; }

		public DomainEvent(IUniqueID aggregateID) {
			AggregateID = aggregateID;
		}
	}
}
