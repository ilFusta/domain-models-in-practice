﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Core {
	public interface IEventStore {
		
		void AddEvent(IDomainEvent e);
		List<IDomainEvent> GetAggregateEvents(IUniqueID aggregateID);
	}
}
