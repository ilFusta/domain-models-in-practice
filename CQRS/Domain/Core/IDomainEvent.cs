﻿using CQRS.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Core {
	public interface IDomainEvent {
		IUniqueID AggregateID { get; }

	}
}
