﻿using CQRS.Domain.Core;
using CQRS.Domain.Screenings.Events;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CQRS.Domain.Screenings {
	class ScreeningReadModel {
		public IEnumerable<Seat> Seats { get; private set; }

		public ScreeningReadModel(IEnumerable<IDomainEvent> events) {
			//ASK what if an unhandled event is found
			foreach (var ev in events) {
				Apply(ev);
			}
		}

		public void Apply(IDomainEvent ev) {
			bool success = ev switch
			{
				ScreeningHasBeenPlanned e => Apply(e),
				SeatsHasBeenReserved e => Apply(e),
				SeatsHasBeenBooked e => Apply(e),
				SeatsReservationHasBeenCancelled e => Apply(e),
				_ => true
			};
		}


		private bool Apply(ScreeningHasBeenPlanned ev) {
			Seats = ev.Seats;
			return true;
		}

		private bool Apply(SeatsHasBeenReserved ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Reserved);
			return true;
		}
		private bool Apply(SeatsHasBeenBooked ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Booked);
			return true;
		}
		private bool Apply(SeatsReservationHasBeenCancelled ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Available);
			return true;
		}
	}
}
