﻿using CQRS.Domain.Core;
using CQRS.Domain.Screenings.Events;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CQRS.Domain.Screenings {
	class ScreeningState {
		public ScreeningID ID { get; private set; }
		public IEnumerable<Seat> Seats { get; private set; }
		public DateTime ProjectionDateTime { get; private set; }

		public ScreeningState(IEnumerable<IDomainEvent> events) {
			//ASK what if an unhandled event is found
			foreach (var ev in events) {
				Apply(ev);
			}
		}

		public void Apply(IDomainEvent ev) {
			bool success = ev switch
			{
				ScreeningHasBeenPlanned e => Apply(e),
				SeatsHasBeenReserved e => Apply(e),
				SeatsHasBeenBooked e => Apply(e),
				SeatsReservationHasBeenCancelled e => Apply(e),
				SeatsAlreadyReserved e => throw new Exception("Seats already reserved"),
				ReservationOutOfTime e => throw new Exception("Reservation out of time"),
				_ => throw new Exception($"Unknown Event {ev.GetType().Name}")
			};
		}

		
		private bool Apply(ScreeningHasBeenPlanned ev) {
			ID = ev.ScreeningID;
			ProjectionDateTime = ev.ProjectionDateTime;
			Seats = ev.Seats;
			return true;
		}

		private bool Apply(SeatsHasBeenReserved ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Reserved);
			return true;
		}
		private bool Apply(SeatsHasBeenBooked ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Booked);
			return true;
		}
		private bool Apply(SeatsReservationHasBeenCancelled ev) {
			Seats.Where(x => ev.Seats.Contains(x.ID)).ToList().ForEach(s => s.Status = SeatStatus.Available);
			return true;
		}
	}
}
