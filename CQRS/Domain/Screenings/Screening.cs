﻿using CQRS.Domain.Core;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings.Events;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CQRS.Domain.Screenings {
	class Screening {
        private readonly ScreeningState _screening;
        private readonly Action<IDomainEvent> _publish;

        public Screening(ScreeningState state, Action<IDomainEvent> publish) {
            _screening = state;
            _publish = publish;
        }

        public void Plan(ScreeningID screeningID, DateTime projectionDateTime, IEnumerable<Seat> seats) {
            _publish(new ScreeningHasBeenPlanned(screeningID, projectionDateTime, seats));
        }

        public void ReserveSeats(CustomerID customerID, IEnumerable<SeatID> seats) {
            //Check seats availability
            bool someUnavailable = _screening.Seats.Any(x => x.Status > SeatStatus.Available && seats.Contains(x.ID));

            if (someUnavailable) {
                _publish(new SeatsAlreadyReserved(_screening.ID, seats));
            } else if (_screening.ProjectionDateTime.AddMinutes(-15) <= DateTime.Now) {
                _publish(new ReservationOutOfTime(_screening.ID, customerID, seats));
            } else {
                _publish(new SeatsHasBeenReserved(customerID, _screening.ID, seats));
			}
        }

        public void BookSeats(CustomerID customerID, IEnumerable<SeatID> seats) {
            //TODO: Check for customer existance and valid seat connected to the client
            _publish(new SeatsHasBeenBooked(customerID, _screening.ID, seats));
        }
    }
}
