﻿
using CQRS.Domain.Core;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System.Collections.Generic;

namespace CQRS.Domain.Screenings.Events {
	public class SeatsAlreadyReserved : DomainEvent {
		public IEnumerable<SeatID> Seats { get; }
		public ScreeningID ScreeningID { get; }
		
		public SeatsAlreadyReserved(ScreeningID screeningID, IEnumerable<SeatID> seats) : base(screeningID) {
			ScreeningID = screeningID;
			Seats = seats;
		}
	}
}
