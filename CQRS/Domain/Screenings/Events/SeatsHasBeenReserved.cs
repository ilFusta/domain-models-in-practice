﻿using CQRS.Domain.Core;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System.Collections.Generic;

namespace CQRS.Domain.Screenings.Events {
	public class SeatsHasBeenReserved: DomainEvent {
		public IEnumerable<SeatID> Seats { get;  }
		public ScreeningID ScreeningID { get;  }
		public CustomerID CustomerID { get;  }

		public SeatsHasBeenReserved(CustomerID customerID, ScreeningID screeningID, IEnumerable<SeatID> seats) : base(screeningID) {
			CustomerID = customerID;
			ScreeningID = screeningID;
			Seats = seats;
		}
	}
}
