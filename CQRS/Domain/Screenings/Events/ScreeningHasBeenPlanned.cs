﻿using CQRS.Domain.Core;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Screenings.Events {
	public class ScreeningHasBeenPlanned : DomainEvent {
		public ScreeningID ScreeningID { get; }
		public DateTime ProjectionDateTime { get; }
		public IEnumerable<Seat> Seats { get; }

		public ScreeningHasBeenPlanned(ScreeningID screeningID, DateTime projectionDateTime, IEnumerable<Seat> seats) : base(screeningID) {
			ScreeningID = screeningID;
			ProjectionDateTime = projectionDateTime;
			Seats = seats;
		}
	}
}
