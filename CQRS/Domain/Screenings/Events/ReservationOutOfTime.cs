﻿using CQRS.Domain.Core;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Screenings.Events {
	public class ReservationOutOfTime : DomainEvent {
		public IEnumerable<SeatID> Seats { get; }
		public ScreeningID ScreeningID { get; }
		public CustomerID CustomerID { get; }

		public ReservationOutOfTime(ScreeningID screeningID, CustomerID customerID, IEnumerable<SeatID> seats) : base(screeningID) {
			ScreeningID = screeningID;
			CustomerID = customerID;
			Seats = seats;
		}
	}
}
