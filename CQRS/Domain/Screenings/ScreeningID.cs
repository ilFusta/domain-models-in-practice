﻿using CQRS.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Screenings {
	public class ScreeningID : UniqueID {

		public ScreeningID(string id) : base(id) {

		}
	}
}
