﻿using CQRS.Domain.Core;

namespace CQRS.Domain.Seats {

	public class SeatID : UniqueID {
		public SeatID(string id) : base(id) {

		}
	}
}
