﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Domain.Seats {

	public enum SeatStatus {
		Available,
		Reserved,
		Booked
	}	

	public class Seat {
		public SeatID ID { get; }
		public SeatStatus Status { get; set; }

		public Seat(SeatID seatID, SeatStatus status = SeatStatus.Available) {
			ID = seatID;
			Status = status;
		}

		public override bool Equals(object ob) {
			if (ob is Seat) {
				Seat s = (Seat)ob;
				return s.ID.ToString() == ID.ToString();
			} else {
				return false;
			}
		}

		public override int GetHashCode() {
			return ID.GetHashCode();
		}

	}
}
