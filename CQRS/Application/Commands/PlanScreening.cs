﻿using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.Commands {
	public class PlanScreening:ICommand {
		public ScreeningID ScreeningID { get; private set; }
		public DateTime PlannedDateTime { get; private set; }
		public IEnumerable<SeatID> Seats { get; private set; }

		public PlanScreening(ScreeningID screeningID, DateTime plannedDateTime, IEnumerable<SeatID> seats) {
			ScreeningID = screeningID;
			PlannedDateTime = plannedDateTime;
			Seats = seats;
		}
	}
}
