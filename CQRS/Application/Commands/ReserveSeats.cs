﻿using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.Commands {
	public class ReserveSeats:ICommand {
		public ScreeningID ScreeningID { get; private set; }
		public IEnumerable<SeatID> Seats { get; private set; }
		public CustomerID CustomerID { get; private set; }

		public ReserveSeats(ScreeningID screeningID, CustomerID customerID, IEnumerable<SeatID> seats) {
			ScreeningID = screeningID;
			CustomerID = customerID;
			Seats = seats;
		}
	}
}
