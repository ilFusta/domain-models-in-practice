﻿using CQRS.Application.Query;
using CQRS.Domain.Core;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CQRS.Application.QueryHandlers {
	class ScreeningQueryHandler:IQueryHandler {
        private readonly IEventStore _eventStore;
        private readonly Action<dynamic> _respond;

        public ScreeningQueryHandler(IEventStore eventStore, Action<dynamic> respond) {
            _eventStore = eventStore;
            _respond = respond;
        }

        public void Handle(IQuery query) {
            switch (query) {
                case GetAvailableSeats qry:
                    Handle(qry);
                    break;
                case GetCustomerActiveReservation cmd:
                    Handle(cmd);
                    break;
                default:
                    throw new Exception("Unrecognized command");
            }
        }

        public void Handle(GetAvailableSeats query) {
            var readModel = new ScreeningReadModel(_eventStore.GetAggregateEvents(query.ScreeningID));

            List<Seat> availableSeats = readModel.Seats.Where(x => x.Status == SeatStatus.Available).ToList();
            _respond(availableSeats);
        }

        public void Handle(GetCustomerActiveReservation query) {
            var readModel = new ScreeningReadModel(_eventStore.GetAggregateEvents(query.ScreeningID));

            List<Seat> CustomerSeats = readModel.Seats.Where(x => x.Status > SeatStatus.Available).ToList();
            _respond(CustomerSeats);
        }



    }
}
