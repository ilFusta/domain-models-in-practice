﻿using CQRS.Application.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.QueryHandlers {
	public interface IQueryHandler {
		void Handle(IQuery query) {

		}
	}
}
