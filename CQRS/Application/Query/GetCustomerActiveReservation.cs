﻿using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.Query {
	class GetCustomerActiveReservation:IQuery {
		public ScreeningID ScreeningID { get; private set; }
		public CustomerID CustomerID { get; }

		public GetCustomerActiveReservation(ScreeningID screeningID, CustomerID customerID) {
			ScreeningID = screeningID;
			CustomerID = customerID;
		}
	}
}
