﻿using CQRS.Domain.Screenings;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.Query {
	class GetAvailableSeats:IQuery {
		public ScreeningID ScreeningID { get; private set; }

		public GetAvailableSeats(ScreeningID screeningID) {
			ScreeningID = screeningID;
		}
	}
}
