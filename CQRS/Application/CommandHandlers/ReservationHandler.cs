﻿using CQRS.Application.Commands;
using CQRS.Domain.Core;
using CQRS.Domain.Screenings;
using System;

namespace CQRS.Application.CommandHandlers {
	class ReservationHandler: ICommandHandler {
        private readonly IEventStore _eventStore;
        private readonly Action<IDomainEvent> _store;

        public ReservationHandler(IEventStore eventStore, Action<IDomainEvent> store) {
            _eventStore = eventStore;
            _store = store;
        }

        public void Handle(ReserveSeats command) {
            var screeningState = new ScreeningState(_eventStore.GetAggregateEvents(command.ScreeningID));
            var screening = new Screening(screeningState, PublishEvent(screeningState));

            screening.ReserveSeats(command.CustomerID, command.Seats);
        }

		public void Handle(ICommand command) {
			
		}

		Action<IDomainEvent> PublishEvent(ScreeningState state) {
            return (IDomainEvent ev) => {
                state.Apply(ev);
                _store(ev);
            };
        }
    }
}
