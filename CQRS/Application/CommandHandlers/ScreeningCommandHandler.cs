﻿using CQRS.Application.Commands;
using CQRS.Domain.Core;
using CQRS.Domain.Screenings;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.CommandHandlers {
	public class ScreeningCommandHandler: ICommandHandler {
        private readonly IEventStore _eventStore; // should be readmodel
        private readonly Action<IDomainEvent> _store;

        public ScreeningCommandHandler(IEventStore eventStore, Action<IDomainEvent> store) {
            _eventStore = eventStore;
            _store = store;
        }

        public void Handle(ICommand command) {
            switch (command) {
                case PlanScreening cmd:
                    Handle(cmd);
                    break;
                case ReserveSeats cmd:
                    Handle(cmd);
                    break;
                default:
                    throw new Exception("Unrecognized command");
            }
        }

        public void Handle(PlanScreening command) {
            var state = new ScreeningState(_eventStore.GetAggregateEvents(command.ScreeningID));
            var screening = new Screening(state, PublishEvent(state));
            List<Seat> seats = new List<Seat>();
            foreach (SeatID seat in command.Seats) {
                seats.Add(new Seat(seat));
            }
            screening.Plan(command.ScreeningID, command.PlannedDateTime, seats);
        }

        public void Handle(ReserveSeats command) {
            var screeningState = new ScreeningState(_eventStore.GetAggregateEvents(command.ScreeningID));
            var screening = new Screening(screeningState, PublishEvent(screeningState));

            screening.ReserveSeats(command.CustomerID, command.Seats);
        }

        Action<IDomainEvent> PublishEvent(ScreeningState state) {
            return (IDomainEvent ev) => {
                state.Apply(ev);
                _store(ev);
            };
        }
    }
}

