﻿using CQRS.Application.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace CQRS.Application.CommandHandlers {
	public interface ICommandHandler {
		void Handle(ICommand command);
	}
}
