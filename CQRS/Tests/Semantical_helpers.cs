﻿using CQRS.Application.Commands;
using CQRS.Domain.CuStomer;
using CQRS.Domain.Screenings;
using CQRS.Domain.Screenings.Events;
using CQRS.Domain.Seats;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CQRS.Tests {
	public partial class Test_base
    {
		// Factory methods for commands, queries and alike. 
		// Easier to read (since the "new" keyword is omitted, and also decouple tests from the actual implementation.
		protected ReserveSeats Reserve_seats(ScreeningID screeningID, CustomerID customerID, IEnumerable<SeatID> seats) {
			return new ReserveSeats(screeningID, customerID, seats);
		}

		protected SeatsHasBeenReserved Seat_has_been_reserved(CustomerID customerID, ScreeningID screeningID, IEnumerable<SeatID> seats) {
			return new SeatsHasBeenReserved(customerID, screeningID, seats);
		}

		protected ScreeningHasBeenPlanned Screening_has_been_planned(ScreeningID screeningID, DateTime projectionDateTime, IEnumerable<Seat> seats) {
			return new ScreeningHasBeenPlanned(screeningID, projectionDateTime, seats);
		}

		protected SeatsAlreadyReserved Seat_already_reserved(ScreeningID screeningID, IEnumerable<SeatID> seats) {
			return new SeatsAlreadyReserved(screeningID, seats);
		}

		// Any Value that is needed to describe a test, but that has no respective ValueType in the domain.
		// Keeping your test clean and easier to write through exploration with intellisense
		protected DateTime December_2nd_2020() {
			//TODO: add time to date
			return new DateTime(2020, 12, 02, 20,30,00);
		}


		// These are Semantic UUID Identifiers for various things in the domain. 
		// It is easier to reason about "Marco" in a test than about "99A83F47..." ;)
		// Pro-Tip: Of course you could use the value type "CustomerReference" instead of a Guid!
		protected CustomerID Marco() {
			return new CustomerID("99A83F47-32D2-4007-A5B8-87C2F1BFD197"); // Reference to the Customer Marco H.
		}

		protected CustomerID Tina() {
			return new CustomerID("BC6BF2B8-A198-4877-980C-E454A4E42628"); // Reference to the Customer Tina F.
		}

		protected Guid Cinema_1() {
			return Guid.Parse("EC15243D-58D5-470D-922D-730FAAFB1B36"); // Reference to Cinema 1 in the first floor
		}

		protected SeatID Seat_A1() {
			return new SeatID("108F0340-C954-44E3-84C2-6CD453351553"); // Reference to the first seat in row A in Cinema 1
		}

		protected SeatID Seat_A2() {
			return new SeatID("4E5DAC6B-E898-4653-91C6-941204E64BFC"); // Reference to the second seat in row A in Cinema 1
		}
		protected SeatID Seat_B1() {
			return new SeatID("5E5DAC6B-E898-4653-91C6-941204E64BFC"); // Reference to the second seat in row A in Cinema 1
		}
		protected SeatID Seat_B2() {
			return new SeatID("6E5DAC6B-E898-4653-91C6-941204E64BFC"); // Reference to the second seat in row A in Cinema 1
		}
		

		protected List<Seat> Room_Seats() {
			return new List<Seat>() { new Seat(Seat_A1()), new Seat(Seat_A2()), new Seat(Seat_B1()), new Seat(Seat_B2()) };
		}

		protected List<SeatID> Seat_Reserved_By_Tina() {
			return new List<SeatID>() { Seat_A1(), Seat_A2()};
		}
		protected List<SeatID> Seat_Reserved_By_Marco() {
			return new List<SeatID>() { Seat_B1(), Seat_B2() };
		}

		protected ScreeningID Screening_1() {
			return new ScreeningID("42F573B3-257B-48C3-BC94-7ACC37C5D3F4"); // Reference to the screening of "Avengers End Game" on december the second.}
		}
	}

}