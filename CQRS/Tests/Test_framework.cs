﻿using CQRS.Application.Commands;
using CQRS.Application.Query;
using CQRS.Domain.Core;
using CQRS.Persistance;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using CommandHandler = CQRS.Application.CommandHandlers.ScreeningCommandHandler;
using QueryHandler = CQRS.Application.QueryHandlers.ScreeningQueryHandler;

namespace CQRS.Tests {
	public partial class Test_base
    {
        private IEventStore _history;
        private List<IDomainEvent> _published_events;
        private object _queried_response;

        [SetUp]
        public void Setup()
        {
            _history = new EventStore();
            _published_events = new List<IDomainEvent>();
            _queried_response = null;
        }



		protected void Given(IEventStore eventStore) {
			_history = eventStore;
		}

		protected void When(ICommand command)
        {
            var handler = new CommandHandler(_history, e => _published_events.Add(e));
            handler.Handle(command);
        }

        protected void When_Query(IQuery query)
        {
            var handler = new QueryHandler(_history, r => _queried_response = r);
            handler.Handle(query);
        }

		protected void Then_expect(List<IDomainEvent> expected_events) {
			_published_events.Should().Equal(expected_events);
		}

		protected void Then_expect_response(object expected_response) {
			_queried_response.Should().BeEquivalentTo(expected_response);
		}
	}

}
