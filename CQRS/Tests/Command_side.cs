using CQRS.Domain.Core;
using CQRS.Persistance;
using NUnit.Framework;
using System.Collections.Generic;

namespace CQRS.Tests
{
    // These are domain-driven tests about the command side of your system
    // Ideally only written in Given / When / Then using Events and Commands
    public class Command_side : Test_base
    {
        [Test]
        public void A_free_seat_can_be_reserved()
        {
            EventStore eventStore = new EventStore(new List<IDomainEvent>() { 
                Screening_has_been_planned(Screening_1(), December_2nd_2020(), Room_Seats()),
                Seat_has_been_reserved(Tina(), Screening_1(), Seat_Reserved_By_Tina())
            });
            
            Given(eventStore);

            When(
                Reserve_seats(Screening_1(), Marco(), Seat_Reserved_By_Marco()) );

            Then_expect(new List<IDomainEvent>() {
                Seat_has_been_reserved(Marco(), Screening_1(), Seat_Reserved_By_Marco()) });
        }

        
        [Test]
        public void An_already_reserved_seat_cannot_be_reserved()
        {
            EventStore eventStore = new EventStore(new List<IDomainEvent>() {
                Screening_has_been_planned(Screening_1(), December_2nd_2020(), Room_Seats()),
                Seat_has_been_reserved(Tina(), Screening_1(), Seat_Reserved_By_Tina())
            });

            Given(eventStore);

            When(
                Reserve_seats(Screening_1(), Marco(), Seat_Reserved_By_Tina()));

            Then_expect(new List<IDomainEvent>() {
                Seat_already_reserved(Screening_1(), Seat_Reserved_By_Tina()) });
        }

        //TODO: test not on time
    }
}